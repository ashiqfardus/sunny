<html>
    <head>
        <title> Welcome to Sunny Apa's Website</title>
        <meta charset="UTF-8">
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/custom.css" type="text/css" media="all">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    </head>
    <body style="background-color:#D3DCE3;">
		<div class="row row_nav">
			<!--Here is navbar-->
			<?php include ('includes/navbar.php') ;?>
		</div>
		<div class="row row_nav1">
			<div class="container">
				<!--Here is Main content-->
				<div class="col-md-7 search_result">
					<?php
						include("includes/connect.php");
						if(isset($_GET['search'])){
							$search_id= $_GET['value'];
							$search_query = "select * from posts where post_keyword like '%$search_id%' or post_title like '%$search_id%' ";
							$run_query = mysql_query($search_query);
							while($search_row = mysql_fetch_array($run_query)){
								$post_id= $search_row['post_id'];
								$post_title = $search_row['post_title'];
								$post_image = $search_row['post_image'];
								$post_content = substr($search_row['post_content'],0,100);
								
							
							?>
							<h1 class"row_nav1"> Here is your search result </h1>
							<h2><a href="pages.php?id=<?php echo $post_id; ?>"><?php if (isset($post_title)){ echo $post_title;} ?></a></h2>
							<img class="image_size" src= "images/<?php echo $post_image; ?>">
							<p><?php if (isset($post_content)){ echo $post_content;} ?></p>
							<p align="right"><a href="pages.php?id=<?php echo $post_id; ?>">Read More</a></p>
						<?php } } ?>
					
				</div>
				<!--Here is Sidebar-->
				<?php include ('includes/sidebar.php') ;?>
			</div>
		</div>
		<div class="row row_nav">
			<div class="container">
			<!--Here is footer-->
			<?php include ('includes/footer.php') ;?>
				
			</div>
		</div>
    </body>
</html>