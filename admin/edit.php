<?php
	session_start();
	if(!isset($_SESSION['username'])){
		header("location: login.php");
	}
	else{
		
	
?>
<html>
	<head>
		<title>Admin Panel </title>
		<meta charset="UTF-8">
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="bootstrap.css">
        <link rel="stylesheet" href="custom.css">
	</head>
	<body class="body_color">
		<div class="row">
		<h3 class="header_style"><a href="index.php">Welcome to Admin Panel </a></h3>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-3">
				<div class="form_align">
					<h2><a class="link_style" href="logout.php"> Logout  </a> </h2>
					<h2><a class="link_style" href="view_posts.php"> View posts  </a> </h2>
					<h2><a class="link_style" href="insert_post.php"> Insert new post </a> </h2>
					<h2><a class="link_style" href=""> View Comment </a> </h2>
				</div>
			</div>
			<?php
				include("includes/connect.php");
				if(isset($_GET['edit'])){
					$edit_id=$_GET['edit'];
					$edit_query= "SELECT * FROM posts WHERE post_id='$edit_id'";
					$run_edit=mysql_query($edit_query);
					while($edit_row= mysql_fetch_array($run_edit)){
						
						$post_id=$edit_row['post_id'];
						$post_title=$edit_row['post_title'];
						$post_author=$edit_row['post_author'];
						$post_image=$edit_row['post_image'];
						$post_keyword=$edit_row['post_keyword'];
						$post_content=$edit_row['post_content'];
						
				
					}
				}
			?>
		
			<div class="col-md-9">
				<form class="form_align" action="edit.php?edit_form=<?php echo $post_id ?>" method="post" enctype="multipart/form-data">
				 <p class="form_head">Post Title: </p> <br>
				  <input class="input_form" type="text" name="title" placeholder="Input Title" value="<?php if (isset($post_title)){ echo $post_title;} ?>"><br>
				  <p class="form_head">Post Author:</p><br>
				  <input class="input_form" type="text" name="author" placeholder="Input Author" value="<?php if (isset($post_author)){ echo $post_author;} ?>"><br>
				  <p class="form_head">Post Keywords: </p><br>
				  <input class="input_form" type="text" name="keyword" placeholder="Input Keywords" value="<?php if (isset($post_keyword)){ echo $post_keyword;} ?>"><br>
				  <p class="form_head">Post Image: </p><br>
				  <center>
					<input class="image_form" type="file" name="image"><br>
				  </center>
				  <img style="height:60px; widhth:80px;" src= "../images/<?php echo $post_image; ?>"><br>
				  <p class="form_head">Post Content: </p><br>
				  <textarea class="textarea_size" name="content" cols="22"> <?php if (isset($post_content)){ echo $post_content;} ?> </textarea><br>
				  <input class="insert_style" type="submit" name="update" class="submit_pad">
				</form>
			</div>
		</div>
						
	</body>
</html>

<?php
	include("includes/connect.php");
	if(isset($_POST['update'])){
		$update_id=$_GET['edit_form'];
		$post_title1=$_POST['title'];
		$post_date1=date('m-d-y');
		$post_author1=$_POST['author'];
		$post_keyword1=$_POST['keyword'];
		$post_content1=$_POST['content'];
		$post_image1=$_FILES['image']['name'];
		$image_tmp=$_FILES['image']['tmp_name'];
		
		if($post_title1=='' or $post_author1=='' or $post_keyword1=='' or $post_content1=='' or $post_image1==''){
			echo "<script>alert('Any of the fields is empty')</script>";
			
			exit();
		}
		else{
			move_uploaded_file($image_tmp,"../images/$post_image1");
			$update_query= "UPDATE posts SET post_title='$post_title1',post_date='$post_date1',post_author='$post_author1',post_image='$post_image1',post_keyword='$post_keyword1',post_content='$post_content1' WHERE post_id='$update_id'" ;
			
			if(mysql_query($update_query)){
			echo "<script> alert('Post updated Successfully.')</script>";
			echo "<script> window.open('view_posts.php','_SELF')</script>";
			echo "<script> window.close()</script>";
			}
		}

	}
?>
<?php } ?>
