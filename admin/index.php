<?php
	session_start();
	if(!isset($_SESSION['username'])){
		header("location: login.php");
	}
	else{
		
	
?>
<html>
	<head>
		<title> Admin panel </title>
		<link rel="stylesheet" href="custom.css">
		<link rel="stylesheet" href="bootstrap.css">
	</head>
	<body class="body_color">
		<div class="row">
		<h3 class="header_style"><a href="index.php">Welcome to Admin Panel </a></h3>
		<h2 class="header_style">Logged in as: <?php echo $_SESSION['username']; ?> </h2>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-3">
				<div class="form_align">
					
					<h2><a class="link_style" href="logout.php"> Logout  </a> </h2>
					<h2><a class="link_style" href="view_posts.php"> View posts  </a> </h2>
					<h2><a class="link_style" href="insert_post.php"> Insert new post </a> </h2>
					<h2><a class="link_style" href=""> View Comment </a> </h2>
				</div>
			</div>
			<div class="col-md-9 ">
				<h2> Welcome to admin panel </h2>
				<h3> Here you can manage your website</h3>
			</div>
		</div>
	</body>
</html>
	<?php } ?>