<?php
	session_start();
	if(!isset($_SESSION['username'])){
		header("location: login.php");
	}
	else{
		
	
?>
<html>
	<head>
		<title>Admin Panel </title>
		<meta charset="UTF-8">
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="bootstrap.css">
        <link rel="stylesheet" href="custom.css">
	</head>
	<body class="body_color">
		<div class="row">
		<h3 class="header_style"><a href="index.php">Welcome to Admin Panel </a></h3>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-3">
				<div class="form_align">
					<h2><a class="link_style" href="logout.php"> Logout  </a> </h2>
					<h2><a class="link_style" href="view_posts.php"> View posts  </a> </h2>
					<h2><a class="link_style" href="insert_post.php"> Insert new post </a> </h2>
					<h2><a class="link_style" href=""> View Comment </a> </h2>
				</div>
			</div>
			<div class="col-md-9 ">
				<div class="form_align">
					<form class="form_align" action="insert_post.php" method="post" enctype="multipart/form-data">
					  <p class="form_head">Post Title: </p> <br>
					  <input class="input_form" type="text" name="title" placeholder="Input Title"><br>
					  <p class="form_head">Post Author:</p><br>
					  <input class="input_form" type="text" name="author" placeholder="Input Author"><br>
					  <p class="form_head">Post Keywords: </p><br>
					  <input class="input_form" type="text" name="keyword" placeholder="Input Keywords"><br>
					  <p class="form_head">Post Image: </p><br>
					  <center>
							<input class="image_form" type="file" name="image"><br>
					  </center>
					  <p class="form_head">Post Content: </p><br>
					  <textarea class="textarea_size" name="content" cols="22"  placeholder="Input Content"></textarea><br>
					  <input class="insert_style" type="submit" name="submit" class="submit_pad">
					</form>
				</div>
			</div>
		</div>
		
	</body>
</html>

<?php
	include("includes/connect.php");
	if(isset($_POST['submit'])){
		$post_title=$_POST['title'];
		$post_date=date('m-d-y');
		$post_author=$_POST['author'];
		$post_keyword=$_POST['keyword'];
		$post_content=$_POST['content'];
		$post_image=$_FILES['image']['name'];
		$image_tmp=$_FILES['image']['tmp_name'];
		
		if($post_title=='' or $post_author=='' or $post_keyword=='' or $post_content==''){
			echo "<script>alert('Any of the fields is empty')</script>";
			
			exit();
		}
		else{
			move_uploaded_file($image_tmp,"../images/$post_image");
			$insert_query="INSERT into posts(post_title,post_date,post_author,post_image,post_keyword,post_content) VALUES('$post_title','$post_date','$post_author','$post_image','$post_keyword','$post_content')";
			
			if(mysql_query($insert_query)){
			echo "<script>alert('Post published successfully.')</script>";
			echo "<script>window.open('view_posts.php','_SELF')</script>";
			}
		}

	}
 } ?>