<?php
	session_start();
	if(!isset($_SESSION['username'])){
		header("location: login.php");
	}
	else{	
?>
<html>
	<head>
		<title> Admin panel </title>
		<link rel="stylesheet" href="custom.css">
		<link rel="stylesheet" href="bootstrap.css">
	</head>
	<body class="body_color">
		<div class="row">
		<h3 class="header_style"><a href="index.php">Welcome to Admin Panel </a></h3>
		</div>
		
		<hr>
		<div class="row">
			<div class="col-md-3">
				<div class="form_align">
					<h2><a class="link_style" href="logout.php"> Logout  </a> </h2>
					<h2><a class="link_style" href="view_posts.php"> View posts  </a> </h2>
					<h2><a class="link_style" href="insert_post.php"> Insert new post </a> </h2>
					<h2><a class="link_style" href=""> View Comment </a> </h2>
				</div>
			</div>
			<div class="col-md-9">
				<div class="">
					  <div class="table-responsive">          
						  <table class="table">
							<thead>
							  <tr>
								<th>Post No</th>
								<th>Post Date</th>
								<th>Post Author</th>
								<th>Post Title</th>
								<th>Post Image</th>
								<th>Post Content</th>
								<th>Delete Post</th>
								<th>Edit Post</th>
								
							  </tr>
							</thead>
							
							<tbody>
							
							  <tr>
							  <?php
								include("includes/connect.php");
								$query= " SELECT * FROM posts ORDER BY 1 DESC";
								$run= mysql_query($query);
								while($row= mysql_fetch_array($run))
								{
									$post_id=$row['post_id'];
									$post_date=$row['post_date'];
									$post_author=$row['post_author'];
									$post_image=$row['post_image'];
									$post_content=substr($row['post_content'],0,100);
									$post_title=$row['post_title'];
									
								
							  ?>
								<td><?php echo $post_id; ?> </td>
								<td><?php echo $post_date; ?></td>
								<td><?php echo $post_author; ?></td>
								<td><?php echo $post_title; ?></td>
								<td><img style="width:80px; height:60px;" src="../images/<?php echo $post_image; ?>"></td>
								<td><?php echo $post_content; ?></td>
								<td><a href="delete.php?del=<?php echo $post_id; ?>">Delete </a></td>
								<td><a href="edit.php?edit=<?php echo $post_id; ?>">Edit </a></td>
								
							  </tr>
							  
							 <?php } ?>
							</tbody>
						
							
						  </table>
						</div>
				</div>
			</div>
		</div>
	</body>
</html>
	<?php } ?>