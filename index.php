<html>
    <head>
        <title> Welcome to Sunny Apa's Website</title>
        <meta charset="UTF-8">
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/custom.css" type="text/css" media="all">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    </head>
    <body style="background-color:#D3DCE3;">
		<div class="row row_nav">
			<!--Here is navbar-->
			<?php include ('includes/navbar.php') ;?>
		</div>
		<div class="row row_nav1">
			<div class="container">
				<!--Here is Main content-->
				<?php include ('includes/content.php') ;?>
				<!--Here is Sidebar-->
				<?php include ('includes/sidebar.php') ;?>
			</div>
		</div>
		<div class="row row_nav">
			<div class="container">
			<!--Here is footer-->
			<?php include ('includes/footer.php') ;?>
				
			</div>
		</div>
    </body>
</html>